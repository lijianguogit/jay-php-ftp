<?php
/**
 * Notes:
 * File name:ftp
 * Create by: Jay.Li
 * Created on: 2021/8/26 0026 17:04
 */

namespace Jay\Ftp\Auth;

use Jay\Ftp\Extensions\FtpException;

class FtpAuth extends AuthAbstract
{
    /**
     * @var
     */
    protected $client;

    /**
     * @var bool
     */
    protected static $instance = false;

    /**
     * @Notes: 返回当前对象实例
     *
     * @param $host
     * @param $port
     * @param $user
     * @param $pass
     * @param int $timeOut
     * @return bool|static
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:21
     */
    public static function getInstance($host,$user, $pass, $port = 21, $timeOut = 90)
    {
        if (self::$instance === false) {
            self::$instance = new static($host, $user, $pass, $port,  $timeOut);
        }

        return self::$instance;
    }

    /**
     * @Notes: 返回ftp链接资源
     *
     * @return mixed
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:19
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @Notes: 设置ftp参数
     *
     * @param $key
     * @param $value
     * @return $this
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:19
     */
    public function setOptions($key, $value)
    {
        $result = ftp_set_option($this->client, $key, $value);

        if ($result === false) {
            throw new FtpException(sprintf('设置当前的ftp资源失败: 当前的资源是 %s:%s, 设置的参数是 %s, 值是 %s', $this->host, $this->port, $key, $value));
        }

        return $this;
    }

    /**
     * @Notes: 得到ftp参数
     *
     * @param $key
     * @return mixed
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:19
     */
    public function getOptions($key)
    {
        $result = ftp_get_option($this->client, $key);

        if ($result === false) {
            throw new FtpException(sprintf('查询ftp参数 %s 出错：%s', $key, '未知错误'));
        }

        return $result;
    }

    /**
     * @Notes: 关闭当前的链接
     *
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:20
     */
    public function close()
    {
        $result = ftp_close($this->client);

        if ($result === false) {
            throw new FtpException(sprintf('关闭当前的ftp资源失败: 当前的资源是 %s:%s', $this->host, $this->port));
        }
    }

    /**
     * @Notes: 链接ftp资源
     *
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:20
     */
    protected function connect()
    {
        $this->client = ftp_connect($this->host, $this->port, $this->timeOut);

        if ($this->client === false) {
            throw new FtpException(sprintf('连接ftp出错：当前的 ip %s, 端口 %s', $this->host, $this->port));
        }
    }

    /**
     * @Notes: 登陆ftp资源
     *
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:21
     */
    protected function login()
    {
        $result = ftp_login($this->client, $this->user, $this->pass);

        if ($result === false) {
            throw new FtpException(sprintf('登陆ftp出错：用户名 %s, 密码 %s', $this->user, $this->pass));
        }
    }

    /**
     * @Notes: 自定义错误处理函数
     *
     * @param $errorCode
     * @param $message
     * @param $file
     * @param $line
     * @param $content
     * @auther: Jay
     * @Date: 2021/8/27 0027
     * @Time: 18:12
     */
    protected function errorHandle($errorCode, $message, $file, $line, $content)
    {
        $message = sprintf("程序发生了错误，错误级别是：%s， 提示消息：%s，错误所在的文件：%s， 行数：%s", $errorCode, $message, $file, $line);

        throw new FtpException($message);
    }
}