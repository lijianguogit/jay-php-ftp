<?php
/**
 * Notes:
 * File name:auth
 * Create by: Jay.Li
 * Created on: 2021/8/26 0026 17:02
 */

namespace Jay\Ftp\Auth;

use Jay\Ftp\Extensions\FtpException;

abstract class AuthAbstract
{
    /**
     * @var
     */
    protected $host;

    /**
     * @var
     */
    protected $port;

    /**
     * @var
     */
    protected $user;

    /**
     * @var
     */
    protected $pass;

    /**
     * @var int
     */
    protected $timeOut = 0;

    /**
     * Ftp constructor.
     * @param $host
     * @param $port
     * @param $user
     * @param $pass
     * @param $timeOut
     */
    protected function __construct($host,$user, $pass, $port, $timeOut)
    {
        set_error_handler([$this, 'errorHandle']);

        $this->checkFtp();

        $this->host = $host;

        $this->port = $port;

        $this->user = $user;

        $this->pass = $pass;

        $this->timeOut = $timeOut;

        $this->connect();

        $this->login();
    }

    /**
     * @Notes: 检测是否存在ftp扩展
     *
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:19
     */
    protected function checkFtp()
    {
        $result = extension_loaded('ftp');

        if ($result === false) {
            throw new FtpException("当前运行的php环境中没有安装ftp扩展");
        }
    }

    abstract protected function login();

    abstract protected function connect();

    abstract public function close();


}