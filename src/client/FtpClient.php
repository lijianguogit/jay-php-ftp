<?php
/**
 * Notes:
 * File name:Ftp
 * Create by: Jay.Li
 * Created on: 2021/8/26 0026 17:12
 */

namespace Jay\Ftp\Client;

use Generator;
use Jay\Ftp\Auth\AuthAbstract;
use Jay\Ftp\Extensions\FtpException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class FtpClient implements FtpInterface
{
    protected $directorySeparator = DIRECTORY_SEPARATOR;

    protected $ftpClient;

    public function __construct(AuthAbstract $auth)
    {
        $this->ftpClient = $auth;

        if (strtoupper($this->getSystemType()) === 'UNIX') {
            $this->directorySeparator = '/';
        }
    }

    /**
     * @Notes:验证本地文件路径
     *
     * @param $localFilePath
     * @param bool $type
     * @return false|resource
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 12:24
     */
    protected function checkPutObject($localFilePath, $type = true)
    {
        if (!file_exists($localFilePath)) {
            throw new FtpException(sprintf("上传的本地文件不存在 path: %s", $localFilePath));
        }

        if (!$type) {
            return false;
        }

        $handle = fopen($localFilePath, 'r');

        if ($handle === false) {
            throw new FtpException(sprintf("打开本地文件句柄失败！ path: %s", $localFilePath));
        }

        return $handle;
    }

    /**
     * @Notes: 同步阻塞上传文件
     *
     * @param $remoteFilePath
     * @param $localFilePath
     * @param int $mode
     * @param int $startPos
     * @return bool
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 12:11
     */
    public function putObject($remoteFilePath, $localFilePath, $mode = self::BINARY, $startPos = 0)
    {
        $this->checkPutObject($localFilePath, false);

        $this->putObjectRemote($remoteFilePath);

        $remoteFilePath = $this->replacePath($remoteFilePath, false);

        $localFilePath = $this->replacePath($localFilePath);

        $result = ftp_put($this->ftpClient->getClient(), $remoteFilePath, $localFilePath, $mode, $startPos);

        if ($result === false) {
            throw new FtpException(sprintf("ftp_put 同步阻塞方式上传文件失败！"));
        }

        return $result;
    }

    /**
     * @Notes:上传一个已经打开的文件到 FTP 服务器
     *
     * @param $remoteFilePath
     * @param $localFilePath
     * @param int $mode
     * @param int $startPos
     * @return bool
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 12:20
     */
    public function putFObject($remoteFilePath, $localFilePath, $mode = self::BINARY, $startPos = 0)
    {
        $handle = $this->checkPutObject($this->replacePath($localFilePath));

        $this->putObjectRemote($remoteFilePath);

        $remoteFilePath = $this->replacePath($remoteFilePath, false);

        $result = ftp_fput($this->ftpClient->getClient(), $remoteFilePath, $handle, $mode, $startPos);

        fclose($handle);

        if ($result === false) {
            throw new FtpException(sprintf("ftp_fput 同步阻塞方式上传文件失败！"));
        }

        return $result;
    }

    /**
     * @Notes: 异步上传
     *
     * @param $remoteFilePath
     * @param $localFilePath
     * @param int $mode
     * @param int $startPos
     * @return int
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 14:43
     */
    public function putNbObject($remoteFilePath, $localFilePath, $mode = self::BINARY, $startPos = 0)
    {
        $this->checkPutObject($localFilePath, false);

        $this->putObjectRemote($remoteFilePath);

        $remoteFilePath = $this->replacePath($remoteFilePath, false);

        $localFilePath = $this->replacePath($localFilePath);

        $result = ftp_nb_put($this->ftpClient->getClient(), $remoteFilePath, $localFilePath, $mode, $startPos);

        $i = 0;
        while ($result === self::MORE_DATA) {
            $i++;

            $result = ftp_nb_continue($this->ftpClient->getClient());
        }

        if ($result === self::FAILED) {
            throw new FtpException(sprintf("ftp_nb_put 异步传输模式上传文件失败！"));
        }

        return $result;
    }

    /**
     * @Notes:异步上传 打开资源类型
     *
     * @param $remoteFilePath
     * @param $localFilePath
     * @param int $mode
     * @param int $startPos
     * @return int
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 18:38
     */
    public function putNbFObject($remoteFilePath, $localFilePath, $mode = self::BINARY, $startPos = 0)
    {
        $localFilePath = $this->replacePath($localFilePath);

        $handle = $this->checkPutObject($localFilePath);

        $this->putObjectRemote($remoteFilePath);

        $remoteFilePath = $this->replacePath($remoteFilePath, false);

        $result = ftp_nb_fput($this->ftpClient->getClient(), $remoteFilePath, $handle, $mode, $startPos);

        $i = 0;
        while ($result === self::MORE_DATA) {
            $i++;

            $result = ftp_nb_continue($this->ftpClient->getClient());
        }

        if ($result === self::FAILED) {
            throw new FtpException(sprintf("ftp_nb_fput 异步传输模式上传文件失败！"));
        }

        return $result;
    }

    /**
     * @Notes: 下载文件到本地
     *
     * @param $remoteFilePath
     * @param $localFilePath
     * @param int $mode
     * @param int $resumePos
     * @auther: Jay
     * @Date: 2021/8/26 0026
     * @Time: 16:48
     * @return bool
     */
    public function getObject($remoteFilePath, $localFilePath, $mode = self::BINARY, $resumePos  = 0)
    {
        $remoteFilePath = $this->replacePath($remoteFilePath, false);

        $localFilePath = $this->replacePath($localFilePath);

        $localDirName = dirname($localFilePath);

        if (!is_dir($localDirName)) {
            \mkdir($localDirName, 0777, true);
        }

        $result = ftp_get($this->ftpClient->getClient(), $localFilePath, $remoteFilePath, $mode, $resumePos);

        if ($result === false) {
            throw new FtpException(sprintf("从当前ftp服务器上下载 %s 文件到 %s 失败！", $remoteFilePath, $localFilePath));
        }

        return $result;
    }

    /**
     * @Notes: 返回目录下的所有目录和文件（child目录）
     *
     * @param $directory
     * @return array|false
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 10:21
     */
    public function getObjectList($directory)
    {
        $result = ftp_nlist($this->ftpClient->getClient(), $directory);

        if ($result === false) {
            throw new FtpException(sprintf("得到当前ftp服务器目录列表失败：目录 %s, 必须是一个相对或绝对路径的空的目录", $directory));
        }

        unset($result[array_search('.', $result)], $result[array_search('..', $result)]);

        return $result;
    }

    /**
     * @Notes: 返回目录下的文件，yield 形式返回
     *
     * @param $directory
     * @return Generator
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 10:20
     */
    public function getObjectListYield($directory)
    {
        if ($result = $this->getObjectList($directory)) {
            foreach ($result as $item) {
                yield $item;
            }
        }
    }

    /**
     * @Notes:得到目录下文件的详细
     *
     * @param $directory
     * @return array
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 10:53
     */
    public function getObjectMlSd($directory)
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            $result = ftp_mlsd($this->ftpClient->getClient(), $directory);

            if ($result === false) {
                throw new FtpException(sprintf("得到目录下文件的详细信息失败！"));
            }

            return $result;
        } else {
            $result = ftp_rawlist($this->ftpClient->getClient(), $directory);

            if ($result === false) {
                throw new FtpException(sprintf("得到目录下文件的详细信息失败！method: %s", 'ftp_rawlist'));
            }

            $data = [];
            foreach ($result as $item) {
                $tmp = preg_split('/[\s]+/', $item, 9, PREG_SPLIT_NO_EMPTY);
                if ($tmp[8] === '.') {
                    $data[] = [
                        'type' => 'cdir',
                        'name' => '.',
                    ];
                } elseif ($tmp[8] === '..') {
                    $data[] = [
                        'type' => 'pdir',
                        'name' => '..',
                    ];
                } else {
                    $data[] = [
                        'type' => preg_match('/^[d]+/', $tmp[0]) ? 'dir' : 'file',
                        'name' => $tmp[8]
                    ];
                }
            }
            unset($result);

            return $data;
        }

        //throw new FtpException(sprintf("此函数 ftp_mlsd() 只有php的版本大于等于7.2.0才可以使用,当前的php版本是 %s", PHP_VERSION));
    }

    /**
     * @Notes: 删除具体的文件
     *
     * @param $path
     * @return bool
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:13
     */
    public function delObject($path)
    {
        $result = ftp_delete($this->ftpClient->getClient(), $path);

        if ($result === false) {
            throw new FtpException(sprintf("删除当前ftp服务器文件失败：文件路径 %s", $path));
        }

        return $result;
    }

    /**
     * @Notes: 创建文件
     *
     * @param $directory
     * @param int $mode
     * @param bool $type
     * @return bool|false|string
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 18:04
     */
    public function mkdir($directory, $mode = 0777, $type = false)
    {
        $directory = $this->replacePath($directory, false);

        $directory = rtrim($directory, $this->directorySeparator);

        $arr = explode($this->directorySeparator, $directory);

        if ($type) {
            $path = '';
            foreach ($arr as $key => $item) {
                if ($this->isWin()) {
                    if ($key === 0) {
                        $path = $item;
                    } else {
                        $path .= '/' . $item;
                    }
                } else {
                    $path .= '/' . $item;
                }

                if (!@ftp_chdir($this->ftpClient->getClient(), $path)) {
                    if (!ftp_mkdir($this->ftpClient->getClient(), $path)) {
                        goto error;
                    }
                }
                @ftp_chmod($this->ftpClient->getClient(), $mode, $path);
            }

            $result = true;
        } else {
            $result = ftp_mkdir($this->ftpClient->getClient(), $directory);
        }

        if ($result === false) {
            error:
            throw new FtpException(sprintf("创建当前ftp服务器目录失败：目录 %s, 必须是一个相对或绝对路径的空的目录", $directory));
        }

        return $result;
    }

    /**
     * @Notes: 递归删除目录和文件
     *
     * @param $directory
     * @param bool $type
     * @return bool
     * @auther: Jay
     * @Date: 2021/8/24 0024
     * @Time: 18:38
     */
    public function rmdir($directory, $type = false)
    {
        $directory = $this->replacePath($directory, false);

        function rmdirS($directory, $client, $that) {
            $lists = $that->getObjectMlSd($directory);

            unset($lists[0], $lists[1]);

            foreach($lists as $list) {
                $full = $directory . $that->getDirectorySeparator() . $list['name'];

                if($list['type'] == 'dir'){
                    rmdirS($full, $client, $that);
                }else{
                    $that->delObject($full);
                }
            }

            return rmdirM($directory, $client);
        }

        function rmdirM($directory, $client) {
            $result = ftp_rmdir($client, $directory);

            if ($result === false) {
                throw new FtpException(sprintf("删除当前ftp服务器目录失败：目录 %s, 必须是一个相对或绝对路径的空的目录", $directory));
            }

            return $result;
        }

        if ($type) {
            return rmdirS($directory, $this->ftpClient->getClient(), $this);
        } else {
            return rmdirM($directory, $this->ftpClient->getClient());
        }
    }

    /**
     * @Notes: 得到文件大小
     *
     * @param $filePath
     * @return int
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:28
     */
    public function getFileSize($filePath)
    {
        $result = ftp_size($this->ftpClient->getClient(), $filePath);

        if ($result === -1) {
            throw new FtpException(sprintf('获取ftp服务器文件 %s 大小失败', $filePath));
        }

        return $result;
    }

    /**
     * @Notes:服务器操作类型
     *
     * @return false|string
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 11:27
     */
    public function getSystemType()
    {
        $result = ftp_systype($this->ftpClient->getClient());

        if ($result === false) {
            throw new FtpException(sprintf("得到远程ftp服务器操作类型失败"));
        }

        return $result;
    }

    public function exec($cmd)
    {
        $result = ftp_site($this->ftpClient->getClient(), $cmd);

        if ($result === false) {
            throw new FtpException(sprintf('执行服务器 site 命令出错：%s', '未知错误'));
        }

        return $this;
    }

    /**
     * @Notes: 远程目录的创建
     *
     * @param $remoteFilePath
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 18:26
     */
    protected function putObjectRemote($remoteFilePath)
    {
        $remoteFilePathDirName = dirname($remoteFilePath);

        $this->mkdir($remoteFilePathDirName, 0777, true);
    }

    /**
     * @Notes: 路径整理
     *
     * @param $path
     * @param bool $type
     * @return string|string[]|null
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 18:16
     */
    protected function replacePath($path, $type = true)
    {
        $replace = $type ? DIRECTORY_SEPARATOR : $this->directorySeparator;

        return preg_replace('/[\\\\|\/]+/', $replace, $path);
    }

    /**
     * @Notes: 客户端系统判断
     *
     * @return bool
     * @auther: Jay
     * @Date: 2021/8/25 0025
     * @Time: 18:16
     */
    protected function isWin()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * @Notes:
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @auther: Jay
     * @Date: 2021/8/26 0026
     * @Time: 10:27
     * @throws ReflectionException
     */
    public function __call($name, $arguments)
    {
        $obj = new ReflectionClass($this);

        $methods = $obj->getMethods(ReflectionMethod::IS_PROTECTED | ReflectionMethod::IS_PRIVATE);

        $methods = json_decode(json_encode($methods), true);

        $methodsValue = array_column($methods, 'name');

        if (array_search($name, $methodsValue, true)) {
            throw new FtpException(sprintf('当前的方法属于受保护的方法，不能在类外面调用， 方法名：%s', $name));
        }

        array_unshift($arguments, $this->ftpClient->getClient());

        if (substr($name, 0, 4) !== 'ftp_') {
            goto error;
        }

        if (!function_exists($name)) {
            goto error;
        }

        $result = call_user_func_array($name, $arguments);

        if ($result === false) {
            error:
            throw new FtpException(sprintf('当前调用的方法不存在或者是参数错误， 方法名：%s， 参数：%s', $name, implode(',', $arguments)));
        }

        return $result;
    }

    public function getDirectorySeparator()
    {
        return $this->directorySeparator;
    }
}