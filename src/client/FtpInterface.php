<?php
/**
 * Notes:
 * File name:FtpClient
 * Create by: Jay.Li
 * Created on: 2021/8/26 0026 17:13
 */

namespace Jay\Ftp\Client;

interface FtpInterface
{
    const TEXT = FTP_ASCII;
    const BINARY = FTP_BINARY;

    const FAILED = FTP_FAILED;
    const FINISHED = FTP_FINISHED;
    const MORE_DATA = FTP_MOREDATA;

    public function putObject($remoteFilePath, $localFilePath, $mode, $startPos);

    public function putFObject($remoteFilePath, $localFilePath, $mode, $startPos);

    public function putNbObject($remoteFilePath, $localFilePath, $mode, $startPos);

    public function putNbFObject($remoteFilePath, $localFilePath, $mode, $startPos);

    public function getObject($remoteFilePath, $localFilePath, $mode, $resumePos);

    public function getObjectList($directory);

    public function getObjectMlSd($directory);

    public function delObject($path);

    public function mkdir($directory, $mode, $type);

    public function rmdir($directory, $type);

    public function getFileSize($filePath);

    public function getSystemType();

    public function exec($cmd);
}