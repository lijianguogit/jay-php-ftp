<?php
/**
 * Notes:
 * File name:${fILE_NAME}
 * Create by: Jay.Li
 * Created on: 2021/8/26 0026 17:21
 */

require __DIR__ . '/../vendor/autoload.php';

use Jay\Ftp\Auth\FtpAuth;

try {
    $auth = FtpAuth::getInstance('***', '**', '**');

    $auth->setOptions(FTP_USEPASVADDRESS, false);
} catch (\Jay\Ftp\Extensions\FtpException $e) {
    var_dump($e->getMessage());
}

