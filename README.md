### 使用php-ftp 客户端

* 安装 ``composer require jayli/ftp``

* 引入类

```php
use Jay\Ftp\Auth\FtpAuth;
use Jay\Ftp\Client\FtpClient;
```

* 登陆

```php
$auth = FtpAuth::getInstance('127.0.0.1', 'root', 'root');

//设置参数
$auth->setOptions(FTP_USEPASVADDRESS, false);
```

* 得到操作客户端

```php
$ftpClient = new FtpClient($auth);
```

* 调用操作方法

    * 上传文件  ```$ftpClient->pubObject('远程目录', '本地文件路径', '传输模式');```
    
    * 下载文件 ``` $ftpClient->getObject('远程目录', '本地文件路径', '传输模式'); ```
    
    * 创建目录 ``` $ftpClient->mkdir('路径', '权限', '是否递归创建'); ```
    
    * 删除目录 ``` $ftpClient->rmdir('路径', '是否递归删除'); ```
    
    * 删除文件 ``` $ftpClient->delObject('文件路径') ```
    
    * 得到目录下文件列表 ``` $ftpClient->getObjectList('路径') ```
    
    * 得到文件大小 ``` $ftpClient->getFileSize('文件路径') ```
    
    * ...